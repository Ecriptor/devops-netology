# devops-netology
## Home work on the course DevOps Enginer

### Будут игнорироваться:
1. Все локальные каталоги и вложенные каталоги с именем .terraform

2. Файлы заканчивающиеся на .tfstate и содержащие в себе .tfstate.

3. Файл с именем crash.log

4. Файлы с именем override.tf, override.tf.json и файлы заканчивающиеся на _override.tf и _override.tf.json

5. Файлы заканчивающиеся на .terraformrc и файл с именем terraform.rc

### Add Remote git_repo + ssh
